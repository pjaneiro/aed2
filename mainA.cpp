#include <cstdlib>
#include <iostream>
#include <string>
#include <cctype>
#include <cstring>
#include <cstdio>

using namespace std;

#define WORDSIZE 256									//tamanho máximo de uma palavra

typedef struct node										//struct usada em cada nó, tem a palavra, a contagem, e ponteiros para os nós filhos e nó pai
{
	string value;
	int count;
	struct node* parent;
	struct node* left;
	struct node* right;
} node;

void toLower(string& key)								//passa uma string para lowercase, quando necessário
{
	for(unsigned int i=0;i<key.size();i++)
	{
		key.at(i) = (char)tolower(key.at(i));
	}
}

class Btree												//Classe principal
{
	public:
		Btree()											//construtor simples, deixa o ponteiro para a raiz com o valor NULL
		{
			this->root = NULL;
		}
		~Btree()										//destrutor, apaga os elementos da árvore 1 a 1
		{
			this->destroyTree();
		}
		void insert(string key)							//dada uma string, insere uma palavra na árvore ou aumenta a sua contagem
		{
			if(root!=NULL)								//se a árvore tiver raiz, corre a função privada de inserção de uma nó, a partir da raiz
			{
				this->insert(key,this->root,NULL);
			}
			else										//se não tiver raiz, cria-a e atribui-lhe a string dada como argumento. inicia a contagem a 1
			{
				this->root = new node;
				toLower(key);
				this->root->value = key;
				this->root->count = 1;
				this->root->parent = NULL;
				this->root->left = NULL;
				this->root->right = NULL;
			}
		}
		node* search(string key)						//dada uma string, devolve um ponteiro para o nó que a contém
		{
			return search(key, this->root);
		}
		void destroyTree()								//destrói a árvore completa
		{
			this->destroyTree(this->root);
		}
		int getWordCount(string key)					//não usada, esta função devolve o número de vezes que uma palavra aparece
		{
			toLower(key);
			node* aux = this->search(key);
			return aux!=NULL?aux->count:0;				//se for diferente de NULL (nó existe), devolve a sua contagem. se não, devolve 0
		}
		void printTree()								//devolve a informação relativa a todos os nós da árvore
		{
			this->printNode(this->root);
		}
		void deleteNode(string key)						//dada uma string, remove o nó que a contém da árvore (se o nó existir)
		{
			this->deleteNode(this->search(key));
		}

	private:
		void destroyTree(node* leaf)					//dado um nó, devolve toda a sua sub-árvore. se for root, destrói toda a árvore
		{
			if(leaf!=NULL)
			{
				this->destroyTree(leaf->left);			//destrói a sub-árvore esquerda
				this->destroyTree(leaf->right);			//destrói a sub-árvore direita
				if(leaf->parent!=NULL)					//para o caso de destruit apenas uma sub-árvore, há que deixar ponteiros a NULL
				{
					if(leaf->parent->left == leaf)
					{
						leaf->parent->left = NULL;
					}
					else if(leaf->parent->right == leaf)
					{
						leaf->parent->right = NULL;
					}
				}
				else									//é possível que se deseje destruir a árvore toda para reconstruir
				{
					this->root = NULL;
				}
				delete leaf;
			}
		}
		void insert(string key, node* leaf, node* parent) //função de inserção, dada uma string e um nó inicial
		{
			toLower(key);
			if(key.compare(leaf->value) < 0)			//se "menor", pertence à sub-árvore esquerda
			{
				if(leaf->left != NULL)					//repete o procedimento no filho à esquerda
				{
					this->insert(key, leaf->left, leaf);
				}
				else									//se não tiver filho à esquerda, cria-o
				{
					leaf->left = new node;
					leaf->left->value = key;
					leaf->left->count = 1;
					leaf->left->parent = leaf;
					leaf->left->left = NULL;
					leaf->left->right = NULL;
				}
			}
			else if(key.compare(leaf->value) > 0)		//se "maior", pertence à sub-árvore direita
			{
				if(leaf->right!=NULL)					//repete o procedimento no filho à direita
				{
					this->insert(key, leaf->right, leaf);
				}
				else									//se não tiver filho à direita, cria-o
				{
					leaf->right = new node;
					leaf->right->value = key;
					leaf->right->count = 1;
					leaf->right->parent = leaf;
					leaf->right->left = NULL;
					leaf->right->right = NULL;
				}
			}
			else										//se é a palavra do nó atual, aumenta a sua contagem
			{
				leaf->count++;
			}
		}
		node* search(string key, node* leaf)			//função de pesquisa, a partir de um dado nó
		{
			toLower(key);
			if(leaf!=NULL)								//se o nó atual existe, compara com a palavra aí existente, e decide o caminho a tomar
			{
				if(key.compare(leaf->value)==0)			//é o nó atual
				{
					return leaf;
				}
				else if(key.compare(leaf->value) < 0)	//pertence à sub-árvore esquerda
				{
					return search(key,leaf->left);
				}
				else									//pertence à sub-árvore direita
				{
					return search(key,leaf->right);
				}
			}
			else										//a palavra não existe na árvore
			{
				return NULL;
			}
		}
		void printNode(node* leaf)						//função simples de impressão a partir de um nó, devolve a informação da sua sub-árvore, ordenada
		{
			if(leaf == NULL)
			{
				return;
			}
			else
			{
				printNode(leaf->left);
				cout << leaf->value << ": " << leaf->count << endl;		//imprime a palavra no nó e a sua contagem
				printNode(leaf->right);
			}
		}
		void deleteNode(node* leaf)						//função de eliminação de um nó
		{
			if(leaf==NULL)								//se o nó não existe, não há nada a eliminar
			{
				return;
			}
			node* aux = leaf->parent;
			if(leaf->left==NULL && leaf->right==NULL)	//não tem filhos
			{
				if(aux == NULL)							//é root
				{
					this->root = NULL;
				}
				else if(aux->left == leaf)				//é filho esquerdo
				{
					aux->left = NULL;
				}
				else									//é filho direito
				{
					aux->right = NULL;
				}
				delete leaf;
			}
			else if(leaf->left==NULL)					//tem filho à direita
			{
				if(aux == NULL)							//é root
				{
					this->root = leaf->right;
					this->root->parent = NULL;
				}
				else if(aux->left == leaf)				//é filho esquerdo
				{
					aux->left = leaf->right;
					aux->left->parent = aux;
				}
				else									//é filho direito
				{
					aux->right = leaf->right;
					aux->right->parent = aux;
				}
				delete leaf;
			}
			else if(leaf->right==NULL)					//tem filho à esquerda
			{
				if(aux == NULL)							//é root
				{
					this->root = leaf->left;
					this->root->parent = NULL;
				}
				else if(aux->left == leaf)					//é filho esquerdo
				{
					aux->left = leaf->left;
					aux->left->parent = aux;
				}
				else									//é filho direito
				{
					aux->right = leaf->left;
					aux->right->parent = aux;
				}
				delete leaf;
			}
			else										//tem dois filhos
			{
				node* sucessor = leaf->right;
				while(sucessor->left != NULL)
				{
					sucessor = sucessor->left;
				}
				leaf->value = sucessor->value;
				leaf->count = sucessor->count;
				this->deleteNode(sucessor);
			}
		}
		node* root;
};

int main(int argc, char* argv[])
{
	Btree tree;												//cria árvore
	string input;											//string para ler cada linha de input
	char* word = (char*)calloc(WORDSIZE,sizeof(char));		//string para cada palavra
	getline(cin,input);										//lê a primeira linha de input
	do
	{
		while(input.size() != 0)							//cada palavra é removida da linha de input, logo no fim a linha vai ter tamanho 0
		{
			char* line = (char*)input.c_str();				//"transforma" a string de C++ numa string clássica
			int result = sscanf(line,"%s",word);			//lê cada palavra
			if(result < 1 || result ==EOF)					//se já não há palavras na linha, passa à linha seguinte
			{
				continue;
			}
			input.erase(0,strlen(word) + 1);				//apaga a palavra lida da linha
			if(strcmp(word,"REMOVE") == 0)					//se for REMOVE, remove a palavra seguinte da árvore
			{
				do
				{
					int toRemove = sscanf(line,"%s",word);	//lê a palavra a remover, se existir
					if(toRemove < 1 || toRemove == EOF)		//se não exitir, REMOVE estava no fim da linha, logo não faz nada
					{
						break;
					}
					input.erase(0,strlen(word) + 1);		//apaga a palavra a remover da linha
				}while(strcmp(word,"REMOVE")==0);			//pode ser "REMOVE REMOVE x"
				tree.deleteNode(word);						//elimina a palavra da árvore binária
			}
			else
			{
				tree.insert(word);							//se não for REMOVE, adiciona a palavra à árvore, ou aumenta a sua contagem
			}
		}
		getline(cin,input);									//lê linha seguinte
	}
	while(input.size()!=0);									//ciclo corre até ler uma linha em branco
	free(word);												//liberta a memória reservada para as palavras
	tree.printTree();										//apresenta os resultados
	return 0;
}
