#include <cstdlib>
#include <iostream>
#include <string>
#include <cctype>
#include <cstring>
#include <cstdio>

using namespace std;

#define WORDSIZE 256									//tamanho máximo de uma palavra

typedef struct node										//struct usada em cada nó, tem a palavra, a contagem, e ponteiros para os nós filhos e nó pai
{
	string value;
	int count;
	int height;
	int factor;
	struct node* parent;
	struct node* left;
	struct node* right;
} node;

void toLower(string& key)								//passa uma string para lowercase, quando necessário
{
	for(unsigned int i=0;i<key.size();i++)
	{
		key.at(i) = (char)tolower(key.at(i));
	}
}

class Btree												//Classe principal
{
	public:
		Btree()											//construtor simples, deixa o ponteiro para a raiz com o valor NULL
		{
			this->root = NULL;
		}
		~Btree()										//destrutor, apaga os elementos da árvore 1 a 1
		{
			this->destroyTree();
		}
		void insert(string key)							//dada uma string, insere uma palavra na árvore ou aumenta a sua contagem
		{
			if(root!=NULL)								//se a árvore tiver raiz, corre a função privada de inserção de uma nó, a partir da raiz
			{
				this->insert(key,this->root,NULL);
			}
			else										//se não tiver raiz, cria-a e atribui-lhe a string dada como argumento. inicia a contagem a 1
			{
				this->root = new node;
				toLower(key);
				this->root->value = key;
				this->root->count = 1;
				this->root->factor = 0;
				this->root->height = 1;
				this->root->parent = NULL;
				this->root->left = NULL;
				this->root->right = NULL;
			}
		}
		node* search(string key)						//dada uma string, devolve um ponteiro para o nó que a contém
		{
			return search(key, this->root);
		}
		void destroyTree()								//destrói a árvore completa
		{
			this->destroyTree(this->root);
		}
		int getWordCount(string key)					//não usada, esta função devolve o número de vezes que uma palavra aparece
		{
			toLower(key);
			node* aux = this->search(key);
			return aux!=NULL?aux->count:0;				//se for diferente de NULL (nó existe), devolve a sua contagem. se não, devolve 0
		}
		void printTree()								//devolve a informação relativa a todos os nós da árvore
		{
			this->printNode(this->root);
		}
		void deleteNode(string key)						//dada uma string, remove o nó que a contém da árvore (se o nó existir)
		{
			this->deleteNode(this->search(key));
		}

	private:
		void destroyTree(node* leaf)					//dado um nó, devolve toda a sua sub-árvore. se for root, destrói toda a árvore
		{
			if(leaf!=NULL)
			{
				this->destroyTree(leaf->left);			//destrói a sub-árvore esquerda
				this->destroyTree(leaf->right);			//destrói a sub-árvore direita
				if(leaf->parent!=NULL)					//para o caso de destruit apenas uma sub-árvore, há que deixar ponteiros a NULL
				{
					if(leaf->parent->left == leaf)
					{
						leaf->parent->left = NULL;
					}
					else if(leaf->parent->right == leaf)
					{
						leaf->parent->right = NULL;
					}
				}
				else									//é possível que se deseje destruir a árvore toda para reconstruir
				{
					this->root = NULL;
				}
				delete leaf;
			}
		}
		void insert(string key, node* leaf, node* parent) //função de inserção, dada uma string e um nó inicial
		{
			toLower(key);
			if(key.compare(leaf->value) < 0)			//se "menor", pertence à sub-árvore esquerda
			{
				if(leaf->left != NULL)					//repete o procedimento no filho à esquerda
				{
					this->insert(key, leaf->left, leaf);
				}
				else									//se não tiver filho à esquerda, cria-o
				{
					leaf->left = new node;
					leaf->left->value = key;
					leaf->left->count = 1;
					leaf->left->height = 1;
					leaf->left->factor = 0;
					leaf->left->parent = leaf;
					leaf->left->left = NULL;
					leaf->left->right = NULL;
					this->checkBalance(leaf->left);
				}
			}
			else if(key.compare(leaf->value) > 0)		//se "maior", pertence à sub-árvore direita
			{
				if(leaf->right!=NULL)					//repete o procedimento no filho à direita
				{
					this->insert(key, leaf->right, leaf);
				}
				else									//se não tiver filho à direita, cria-o
				{
					leaf->right = new node;
					leaf->right->value = key;
					leaf->right->count = 1;
					leaf->right->height = 1;
					leaf->right->factor = 0;
					leaf->right->parent = leaf;
					leaf->right->left = NULL;
					leaf->right->right = NULL;
					this->checkBalance(leaf->right);
				}
			}
			else										//se é a palavra do nó atual, aumenta a sua contagem
			{
				leaf->count++;
			}
		}
		node* search(string key, node* leaf)			//função de pesquisa, a partir de um dado nó
		{
			toLower(key);
			if(leaf!=NULL)								//se o nó atual existe, compara com a palavra aí existente, e decide o caminho a tomar
			{
				if(key.compare(leaf->value)==0)			//é o nó atual
				{
					return leaf;
				}
				else if(key.compare(leaf->value) < 0)	//pertence à sub-árvore esquerda
				{
					return search(key,leaf->left);
				}
				else									//pertence à sub-árvore direita
				{
					return search(key,leaf->right);
				}
			}
			else										//a palavra não existe na árvore
			{
				return NULL;
			}
		}
		void printNode(node* leaf)						//função simples de impressão a partir de um nó, devolve a informação da sua sub-árvore, ordenada
		{
			if(leaf == NULL)
			{
				return;
			}
			else
			{
				printNode(leaf->left);
				cout << leaf->value << ": " << leaf->count << endl; 	//imprime a palavra no nó e a sua contagem
				//cout << leaf->value << " C" << leaf->count << " H" << leaf->height << " F" << leaf->factor << endl;
				printNode(leaf->right);
			}
		}
		void deleteNode(node* leaf)						//função de eliminação de um nó
		{
			if(leaf==NULL)								//se o nó não existe, não há nada a eliminar
			{
				return;
			}
			node* aux = leaf->parent;
			if(leaf->left==NULL && leaf->right==NULL)	//não tem filhos
			{
				if(aux == NULL)							//é root
				{
					this->root = NULL;
				}
				else if(aux->left == leaf)				//é filho esquerdo
				{
					aux->left = NULL;
				}
				else									//é filho direito
				{
					aux->right = NULL;
				}
				delete leaf;
				this->checkBalance(aux);
			}
			else if(leaf->left==NULL)					//tem filho à direita
			{
				if(aux == NULL)							//é root
				{
					this->root = leaf->right;
					this->root->parent = NULL;
				}
				else if(aux->left == leaf)				//é filho esquerdo
				{
					aux->left = leaf->right;
					aux->left->parent = aux;
				}
				else									//é filho direito
				{
					aux->right = leaf->right;
					aux->right->parent = aux;
				}
				delete leaf;
				this->checkBalance(aux);
			}
			else if(leaf->right==NULL)					//tem filho à esquerda
			{
				if(aux == NULL)							//é root
				{
					this->root = leaf->left;
					this->root->parent = NULL;
				}
				else if(aux->left == leaf)					//é filho esquerdo
				{
					aux->left = leaf->left;
					aux->left->parent = aux;
				}
				else									//é filho direito
				{
					aux->right = leaf->left;
					aux->right->parent = aux;
				}
				delete leaf;
				this->checkBalance(aux);
			}
			else										//tem dois filhos
			{
				node* sucessor = leaf->right;
				while(sucessor->left != NULL)
				{
					sucessor = sucessor->left;
				}
				leaf->value = sucessor->value;
				leaf->count = sucessor->count;
				this->deleteNode(sucessor);
			}
		}
		void rotateRight(node* leaf)					//rotação com o filho da direita
		{
			if(leaf==NULL || leaf->right==NULL)			//se o nó ou o filho da direita não existir, aborta
			{
				return;
			}
			node* aux = leaf->right;
			leaf->right = aux->left;
			if(leaf->right!=NULL)						//usando um ponteiro para parent, é importante atualizar o parent da sub-árvore que mudou de sítio
			{
				leaf->right->parent = leaf;
			}
			aux->left = leaf;
			aux->parent = leaf->parent;
			leaf->parent = aux;
			if(aux->parent == NULL)						//verifica se passa a root
			{
				this->root = aux;
			}
			else if(aux->parent->left == leaf)			//filho esquerdo
			{
				aux->parent->left = aux;
			}
			else if(aux->parent->right == leaf)			//filho direito
			{
				aux->parent->right = aux;
			}
		}
		void rotateLeft(node* leaf)						//rotação com o filho da esquerda
		{
			if(leaf==NULL || leaf->left==NULL)			//se o nó ou o filho da esquerda não existir, aborta
			{
				return;
			}
			node* aux = leaf->left;
			leaf->left = aux->right;
			if(leaf->left!=NULL)						//usando um ponteiro para parent, é importante atualizar o parent da sub-árvore que mudou de sítio
			{
				leaf->left->parent = leaf;
			}
			aux->right = leaf;
			aux->parent = leaf->parent;
			leaf->parent = aux;
			if(aux->parent == NULL)						//verifica se passa a root
			{
				this->root = aux;
			}
			else if(aux->parent->left == leaf)			//filho esquerdo
			{
				aux->parent->left = aux;
			}
			else if(aux->parent->right == leaf)			//filho direito
			{
				aux->parent->right = aux;
			}
		}
		void updateHeight(node* leaf)					//atualiza a altura de um nó, de acordo com as alturas das sub-árvores
		{
			if(leaf == NULL)							//para o caso de a função ser chamada indevidamente
			{
				return;
			}
			if(leaf->left==NULL && leaf->right==NULL)	//é uma folha, altura 1
			{
				leaf->height = 1;
			}
			else if(leaf->left==NULL)					//tem apenas filhos à direita
			{
				leaf->height = leaf->right->height+1;
			}
			else if(leaf->right==NULL)					//tem apenas filhos à esquerda
			{
				leaf->height = leaf->left->height+1;
			}
			else										//tem filhos à esquerda e à direita, calcula a altura máxima e soma 1
			{
				leaf->height = (leaf->left->height > leaf->right->height ? leaf->left->height : leaf->right->height) + 1;
			}
		}
		void updateFactor(node* leaf)					//atualiza o factor de equilíbrio de um nó
		{
			if(leaf==NULL)								//para o caso de a função ser chamada indevidamente
			{
				return;
			}
			if(leaf->left==NULL && leaf->right==NULL)	//se é folha, factor 0
			{
				leaf->factor = 0;
			}
			else if(leaf->left==NULL)					//se só tem filhos à direita, o factor de equilíbrio é dado considerando a altura dessa sub-árvore
			{
				leaf->factor = -leaf->right->height;
			}
			else if(leaf->right==NULL)					//se só tem filhos à esquerda, o factor de equilíbrio é dado considerando a altura dessa sub-árvore
			{
				leaf->factor = leaf->left->height;
			}
			else										//tendo dois filhos, é necessário considerar as alturas das duas sub-árvores
			{
				leaf->factor = leaf->left->height-leaf->right->height;
			}
		}
		void checkBalance(node* leaf)					//percorre todos os nós, calculando a sua altura e factor, realizando os ajustes necessários
		{
			while(leaf!=NULL)							//até chegar à raiz
			{
				this->updateHeight(leaf);				//atualiza altura
				this->updateFactor(leaf);				//atualiza factor de equilíbrio
				if(leaf->factor == 2)					//sub-árvore esquerda demasiado "pesada"
				{
					if(leaf->left->factor == 1)			//simples rotação com o filho da esquerda		c-b-a
					{
						this->rotateLeft(leaf);
						continue;
					}
					else if(leaf->left->factor == -1)	//rotação do filho esquerdo à direita, seguida de rotação do nó à esquerda		c-a-b
					{
						node* aux = leaf->left;
						this->rotateRight(aux);
						this->updateHeight(aux);
						this->updateFactor(aux);
						this->rotateLeft(leaf);
						continue;
					}
				}
				else if(leaf->factor == -2)				//sub-árvore direita demasiado pesada
				{
					if(leaf->right->factor == -1)		//simples rotação com o filho da direita		a-b-c
					{
						this->rotateRight(leaf);
						continue;
					}
					else if(leaf->right->factor == 1)	//rotação do filho direito à esquerda, seguida de rotação do nó à direita
					{
						node* aux = leaf->right;
						this->rotateLeft(aux);
						this->updateHeight(aux);
						this->updateFactor(aux);
						this->rotateRight(leaf);
						continue;
					}
				}
				leaf = leaf->parent;					//passa ao nó seguinte
			}
		}
		node* root;
};

int main(int argc, char* argv[])
{
	Btree tree;												//cria árvore
	string input;											//string para ler cada linha de input
	char* word = (char*)calloc(WORDSIZE,sizeof(char));		//string para cada palavra
	getline(cin,input);										//lê a primeira linha de input
	do
	{
		while(input.size() != 0)							//cada palavra é removida da linha de input, logo no fim a linha vai ter tamanho 0
		{
			char* line = (char*)input.c_str();				//"transforma" a string de C++ numa string clássica
			int result = sscanf(line,"%s",word);			//lê cada palavra
			if(result < 1 || result ==EOF)					//se já não há palavras na linha, passa à linha seguinte
			{
				continue;
			}
			input.erase(0,strlen(word) + 1);				//apaga a palavra lida da linha
			if(strcmp(word,"REMOVE") == 0)					//se for REMOVE, remove a palavra seguinte da árvore
			{
				do
				{
					int toRemove = sscanf(line,"%s",word);	//lê a palavra a remover, se existir
					if(toRemove < 1 || toRemove == EOF)		//se não exitir, REMOVE estava no fim da linha, logo não faz nada
					{
						break;
					}
					input.erase(0,strlen(word) + 1);		//apaga a palavra a remover da linha
				}while(strcmp(word,"REMOVE")==0);			//pode ser "REMOVE REMOVE x"
				tree.deleteNode(word);						//elimina a palavra da árvore binária
			}
			else
			{
				tree.insert(word);							//se não for REMOVE, adiciona a palavra à árvore, ou aumenta a sua contagem
				//tree.printTree();
			}
		}
		getline(cin,input);									//lê linha seguinte
	}
	while(input.size()!=0);									//ciclo corre até ler uma linha em branco
	free(word);												//liberta a memória reservada para as palavras
	tree.printTree();										//apresenta os resultados
	return 0;
}
