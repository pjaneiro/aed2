#include <cstdlib>
#include <iostream>
#include <string>
#include <cctype>
#include <cstring>
#include <cstdio>
#include <ctime>

#define WORDSIZE 256

using namespace std;

typedef struct infoNode
{
	string value;
	int count;
} infoNode;

typedef struct posNode
{
	infoNode* node;
	posNode* left;
	posNode* right;
	posNode* up;
	posNode* down;
} posNode;

void toLower(string& key)								//passa uma string para lowercase, quando necessário
{
	for(unsigned int i=0;i<key.size();i++)
	{
		key.at(i) = (char)tolower(key.at(i));
	}
}


class SkipList
{
public:
	SkipList()
	{
		srand(time(0));
		this->root = new posNode;
		this->end = new posNode;

		this->top = this->root;

		this->root->left = 0;
		this->root->right = this->end;
		this->root->up = 0;
		this->root->down = 0;
		this->root->node = 0;
		
		this->end->left = this->root;
		this->end->right = 0;
		this->end->up = 0;
		this->end->down = 0;
		this->end->node = 0;
	}
	~SkipList()
	{
		this->destroyList(this->root);
	}
	void insertNode(string key)
	{
		toLower(key);
		this->insertNode(key,this->top);
	}
	posNode* search(string key)
	{
		toLower(key);
		return this->search(key, this->top);
	}
	void deleteNode(string key)
	{
		toLower(key);
		this->deleteNode(this->search(key));
	}
	void printList()
	{
		this->printNode(this->root);
	}
private:
	void insertNode(string key, posNode* start)
	{
		if(start==0 || start->right==0)
		{
			return;
		}
		if(start->node==0 || start->node->value<=key)										//Nó atual menor ou igual
		{
			if(start->node!=0 && start->node->value==key)										//Nó atual igual
			{
				start->node->count++;
				return;
			}
			else if(start->right->node!=0 && start->right->node->value<=key)					//Nó seguinte menor
			{
				this->insertNode(key,start->right);
				return;
			}
			else if(start->right->node==0 || start->right->node->value>key)						//Nó seguinte não existe ou é maior
			{
				if(start->down!=0)																//Anda para baixo e repete
				{
					this->insertNode(key,start->down);
					return;
				}
				else																			//lista básica
				{
					infoNode* info = new infoNode;
					info->value = key;
					info->count = 1;
					posNode* aux = new posNode;
					aux->node = info;
					aux->left = start;
					aux->right = start->right;
					aux->down = 0;
					aux->up = 0;
					start->right = aux;
					aux->right->left = aux;
					posNode* baseLeft = this->root;
					posNode* baseRight = this->end;
					while(rand()%2 == 0)
					{
						if(baseLeft->up == 0 && baseRight->up == 0)								//só existe uma lista ou é preciso criar mais um nó
						{
							baseLeft->up = new posNode;
							baseLeft->up->down = baseLeft;
							baseRight->up = new posNode;
							baseRight->up->down = baseRight;
							aux->up = new posNode;
							aux->up->down = aux;
							baseLeft = baseLeft->up;
							baseRight = baseRight->up;
							aux = aux->up;
							baseLeft->node = 0;
							baseLeft->left = 0;
							baseLeft->up = 0;
							baseLeft->right = aux;
							aux->node = info;
							aux->left = baseLeft;
							aux->right = baseRight;
							aux->up = 0;
							baseRight->node = 0;
							baseRight->left = aux;
							baseRight->right = 0;
							baseRight->up = 0;
							this->top = baseLeft;
						}
						else
						{
							baseLeft = baseLeft->up;
							baseRight = baseRight->up;
							posNode* auxLeft = baseLeft;
							posNode* auxRight = baseRight;
							while(auxLeft->right->node!=0 && auxLeft->right->node->value<key)
							{
								auxLeft=auxLeft->right;
							}
							while(auxRight->left->node!=0 && auxRight->left->node->value>key)
							{
								auxRight=auxRight->left;
							}
							aux->up = new posNode;
							aux->up->node = info;
							aux->up->down = aux;
							aux->up->up=0;
							aux->up->left = auxLeft;
							aux->up->right = auxRight;
							auxLeft->right = aux->up;
							auxRight->left = aux->up;
							aux = aux->up;
						}
					}
					return;
				}
			}
		}
	}
	posNode* search(string key, posNode* node)
	{
		while(node!=0)
		{
			if(node->node!=0 && node->node->value == key)
			{
				while(node->down!=0)
				{
					node=node->down;
				}
				return node;
			}
			else if(node->right!=0 && node->right->node!=0 && node->right->node->value<=key)
			{
				node = node->right;
				continue;
			}
			else
			{
				node = node->down;
				continue;
			}
		}
		return 0;
	}
	void deleteNode(posNode* node)
	{
		if(node==0)
		{
			return;
		}
		while(node->up!=0)
		{
			node = node->up;
		}
		posNode* aux;
		while(node!=0)
		{
			node->left->right = node->right;
			node->right->left = node->left;
			if(node->left->node==0 && node->right->node==0)
			{
				if(node->left!=this->root)
				{
					node->left->down->up = 0;
					node->right->down->up = 0;
					this->top = this->top->down;
					delete node->left;
					delete node->right;
				}
			}
			aux = node;
			node = node->down;
			delete aux;
		}
	}
	void printNode(posNode* node)
	{
		if(node!=0 && node->node!=0)
		{
			cout << node->node->value << ": " << node->node->count << endl;
		}
		if(node->right!=0)
		{
			this->printNode(node->right);
		}
	}
	void destroyList(posNode* start)
	{
		if(start!=0)
		{
			this->destroyList(start->up);
			posNode* aux;
			while(start!=0)
			{
				aux = start->right;
				delete start;
				start = aux;
			}
		}
	}
	posNode* root;
	posNode* end;
	posNode* top;
};

int main(int argc, char* argv[])
{
	SkipList list;												//cria árvore
	string input;											//string para ler cada linha de input
	char* word = (char*)calloc(WORDSIZE,sizeof(char));		//string para cada palavra
	getline(cin,input);										//lê a primeira linha de input
	do
	{
		while(input.size() != 0)							//cada palavra é removida da linha de input, logo no fim a linha vai ter tamanho 0
		{
			char* line = (char*)input.c_str();				//"transforma" a string de C++ numa string clássica
			int result = sscanf(line,"%s",word);			//lê cada palavra
			if(result < 1 || result ==EOF)					//se já não há palavras na linha, passa à linha seguinte
			{
				continue;
			}
			input.erase(0,strlen(word) + 1);				//apaga a palavra lida da linha
			if(strcmp(word,"REMOVE") == 0)					//se for REMOVE, remove a palavra seguinte da árvore
			{
				do
				{
					int toRemove = sscanf(line,"%s",word);	//lê a palavra a remover, se existir
					if(toRemove < 1 || toRemove == EOF)		//se não exitir, REMOVE estava no fim da linha, logo não faz nada
					{
						break;
					}
					input.erase(0,strlen(word) + 1);		//apaga a palavra a remover da linha
				}while(strcmp(word,"REMOVE")==0);			//pode ser "REMOVE REMOVE x"
				list.deleteNode(word);						//elimina a palavra da árvore binária
				//list.printList();
			}
			else
			{
				list.insertNode(word);							//se não for REMOVE, adiciona a palavra à árvore, ou aumenta a sua contagem
				//list.printList();
			}
		}
		getline(cin,input);									//lê linha seguinte
	}
	while(input.size()!=0);									//ciclo corre até ler uma linha em branco
	free(word);												//liberta a memória reservada para as palavras
	list.printList();										//apresenta os resultados
	return 0;
}
