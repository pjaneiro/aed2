#include <cstdlib>
#include <iostream>
#include <string>
#include <cctype>
#include <cstring>
#include <cstdio>

// 1) - Cada nó é colorido de vermelho ou preto.
// 2) - A raiz é colorida de preto.
// 3) - Se um nó é vermelho os seus filhos são coloridos de preto.
// 4) - Todos os caminhos de cada nó até às suas folhas têm o mesmo número de nós pretos.

using namespace std;

#define WORDSIZE 256									//tamanho máximo de uma palavra
#define RED false
#define BLACK true

typedef struct node										//struct usada em cada nó, tem a palavra, a contagem, e ponteiros para os nós filhos e nó pai
{
	string value;
	int count;
	bool colour;
	struct node* parent;
	struct node* left;
	struct node* right;
} node;

void toLower(string& key)								//passa uma string para lowercase, quando necessário
{
	for(unsigned int i=0;i<key.size();i++)
	{
		key.at(i) = (char)tolower(key.at(i));
	}
}

class Btree												//Classe principal
{
	public:
		Btree()											//construtor simples, deixa o ponteiro para a raiz com o valor NULL
		{
			this->root = NULL;
		}
		~Btree()										//destrutor, apaga os elementos da árvore 1 a 1
		{
			this->destroyTree();
		}
		void insert(string key)							//dada uma string, insere uma palavra na árvore ou aumenta a sua contagem
		{
			if(root!=NULL)								//se a árvore tiver raiz, corre a função privada de inserção de uma nó, a partir da raiz
			{
				this->insert(key,this->root,NULL);
			}
			else										//se não tiver raiz, cria-a e atribui-lhe a string dada como argumento. inicia a contagem a 1
			{
				this->root = new node;
				toLower(key);
				this->root->value = key;
				this->root->count = 1;
				this->root->colour = BLACK;
				this->root->parent = NULL;
				this->root->left = NULL;
				this->root->right = NULL;
			}
		}
		node* search(string key)						//dada uma string, devolve um ponteiro para o nó que a contém
		{
			return search(key, this->root);
		}
		void destroyTree()								//destrói a árvore completa
		{
			this->destroyTree(this->root);
		}
		int getWordCount(string key)					//não usada, esta função devolve o número de vezes que uma palavra aparece
		{
			toLower(key);
			node* aux = this->search(key);
			return aux!=NULL?aux->count:0;				//se for diferente de NULL (nó existe), devolve a sua contagem. se não, devolve 0
		}
		void printTree()								//devolve a informação relativa a todos os nós da árvore
		{
			this->printNode(this->root);
		}
		void deleteNode(string key)						//dada uma string, remove o nó que a contém da árvore (se o nó existir)
		{
			this->deleteNode(this->search(key));
		}

	private:
		void destroyTree(node* leaf)					//dado um nó, devolve toda a sua sub-árvore. se for root, destrói toda a árvore
		{
			if(leaf!=NULL)
			{
				this->destroyTree(leaf->left);			//destrói a sub-árvore esquerda
				this->destroyTree(leaf->right);			//destrói a sub-árvore direita
				if(leaf->parent!=NULL)					//para o caso de destruit apenas uma sub-árvore, há que deixar ponteiros a NULL
				{
					if(leaf->parent->left == leaf)
					{
						leaf->parent->left = NULL;
					}
					else if(leaf->parent->right == leaf)
					{
						leaf->parent->right = NULL;
					}
				}
				else									//é possível que se deseje destruir a árvore toda para reconstruir
				{
					this->root = NULL;
				}
				delete leaf;
			}
		}
		void insert(string key, node* leaf, node* parent) //função de inserção, dada uma string e um nó inicial
		{
			toLower(key);
			if(key.compare(leaf->value) < 0)			//se "menor", pertence à sub-árvore esquerda
			{
				if(leaf->left != NULL)					//repete o procedimento no filho à esquerda
				{
					this->insert(key, leaf->left, leaf);
				}
				else									//se não tiver filho à esquerda, cria-o
				{
					leaf->left = new node;
					leaf->left->value = key;
					leaf->left->count = 1;
					leaf->left->colour = RED;
					leaf->left->parent = leaf;
					leaf->left->left = NULL;
					leaf->left->right = NULL;
					this->checkBalanceInsert(leaf->left);
				}
			}
			else if(key.compare(leaf->value) > 0)		//se "maior", pertence à sub-árvore direita
			{
				if(leaf->right!=NULL)					//repete o procedimento no filho à direita
				{
					this->insert(key, leaf->right, leaf);
				}
				else									//se não tiver filho à direita, cria-o
				{
					leaf->right = new node;
					leaf->right->value = key;
					leaf->right->count = 1;
					leaf->right->colour = RED;
					leaf->right->parent = leaf;
					leaf->right->left = NULL;
					leaf->right->right = NULL;
					this->checkBalanceInsert(leaf->right);
				}
			}
			else										//se é a palavra do nó atual, aumenta a sua contagem
			{
				leaf->count++;
			}
		}
		node* search(string key, node* leaf)			//função de pesquisa, a partir de um dado nó
		{
			toLower(key);
			if(leaf!=NULL)								//se o nó atual existe, compara com a palavra aí existente, e decide o caminho a tomar
			{
				if(key.compare(leaf->value)==0)			//é o nó atual
				{
					return leaf;
				}
				else if(key.compare(leaf->value) < 0)	//pertence à sub-árvore esquerda
				{
					return search(key,leaf->left);
				}
				else									//pertence à sub-árvore direita
				{
					return search(key,leaf->right);
				}
			}
			else										//a palavra não existe na árvore
			{
				return NULL;
			}
		}
		void printNode(node* leaf)						//função simples de impressão a partir de um nó, devolve a informação da sua sub-árvore, ordenada
		{
			if(leaf == NULL)
			{
				return;
			}
			else
			{
				printNode(leaf->left);
				cout << leaf->value << ": " << leaf->count << endl;		//imprime a palavra no nó e a sua contagem
				printNode(leaf->right);
			}
		}
		void deleteNode(node* leaf)						//função de eliminação de um nó
		{
			if(leaf==NULL)								//se o nó não existe, não há nada a eliminar
			{
				return;
			}
			node* aux = leaf->parent;
			if(leaf->left==NULL && leaf->right==NULL)	//não tem filhos
			{
				this->checkBalanceDeletion(leaf);
				if(aux == NULL)							//é root
				{
					this->root = NULL;
				}
				else if(aux->left == leaf)				//é filho esquerdo
				{
					aux->left = NULL;
				}
				else if(aux->right == leaf)				//é filho direito
				{
					aux->right = NULL;
				}
				delete leaf;
			}
			else if(leaf->left==NULL)					//tem filho à direita
			{
				this->checkBalanceDeletion(leaf);
				if(aux == NULL)							//é root
				{
					this->root = leaf->right;
					this->root->parent = NULL;
				}
				else if(aux->left == leaf)				//é filho esquerdo
				{
					aux->left = leaf->right;
					aux->left->parent = aux;
				}
				else if(aux->right == leaf)				//é filho direito
				{
					aux->right = leaf->right;
					aux->right->parent = aux;
				}
				delete leaf;
			}
			else if(leaf->right==NULL)					//tem filho à esquerda
			{
				this->checkBalanceDeletion(leaf);
				if(aux == NULL)							//é root
				{
					this->root = leaf->left;
					this->root->parent = NULL;
				}
				else if(aux->left == leaf)				//é filho esquerdo
				{
					aux->left = leaf->left;
					aux->left->parent = aux;
				}
				else if(aux->right == leaf)				//é filho direito
				{
					aux->right = leaf->left;
					aux->right->parent = aux;
				}
				delete leaf;
			}
			else										//tem dois filhos
			{
				node* sucessor = leaf->right;
				while(sucessor->left != NULL)
				{
					sucessor = sucessor->left;
				}
				leaf->value = sucessor->value;
				leaf->count = sucessor->count;
				this->deleteNode(sucessor);
			}
		}
		void rotateRight(node* leaf)					//rotação com o filho da direita
		{
			if(leaf==NULL || leaf->right==NULL)			//se o nó ou o filho da direita não existir, aborta
			{
				return;
			}
			node* aux = leaf->right;
			leaf->right = aux->left;
			if(leaf->right!=NULL)						//usando um ponteiro para parent, é importante atualizar o parent da sub-árvore que mudou de sítio
			{
				leaf->right->parent = leaf;
			}
			aux->left = leaf;
			aux->parent = leaf->parent;
			leaf->parent = aux;
			if(aux->parent == NULL)						//verifica se passa a root
			{
				this->root = aux;
			}
			else if(aux->parent->left == leaf)			//filho esquerdo
			{
				aux->parent->left = aux;
			}
			else if(aux->parent->right == leaf)			//filho direito
			{
				aux->parent->right = aux;
			}
		}
		void rotateLeft(node* leaf)						//rotação com o filho da esquerda
		{
			if(leaf==NULL || leaf->left==NULL)			//se o nó ou o filho da esquerda não existir, aborta
			{
				return;
			}
			node* aux = leaf->left;
			leaf->left = aux->right;
			if(leaf->left!=NULL)						//usando um ponteiro para parent, é importante atualizar o parent da sub-árvore que mudou de sítio
			{
				leaf->left->parent = leaf;
			}
			aux->right = leaf;
			aux->parent = leaf->parent;
			leaf->parent = aux;
			if(aux->parent == NULL)						//verifica se passa a root
			{
				this->root = aux;
			}
			else if(aux->parent->left == leaf)			//filho esquerdo
			{
				aux->parent->left = aux;
			}
			else if(aux->parent->right == leaf)			//filho direito
			{
				aux->parent->right = aux;
			}
		}
		node* getSibling(node* leaf)					//devolve ponteiro para o irmão de um nó, se existir
		{
			if(leaf==NULL || leaf->parent==NULL)		//nó não existe ou pai não existe
			{
				return NULL;
			}
			else if(leaf->parent->left==leaf)			//se nó atual for filho esquerdo, devolve ponteiro para o direito
			{
				return leaf->parent->right;
			}
			else if(leaf->parent->right==leaf)			//se nó atual for filho direito, devolve ponteiro para o esquerdo
			{
				return leaf->parent->left;
			}
			else										//caso algo corra mal
			{
				return NULL;
			}
		}
		void checkBalanceInsert(node* leaf)				//função que verifica o equilíbrio da árvore após inserção de um nó
		{
			node* current = leaf;
			if(leaf->parent!=NULL && leaf->parent->colour==RED)
			{
				//a verificação começa no pai do nó inserido
				current = leaf->parent;
			}
			while(current!=NULL)
			{
				if(current == this->root)
				{
					//Check to see if there is still a problem
					//sendo a raiz, muda a sua cor para preto
					current->colour = BLACK;
					return;
				}
				else if(current->parent->colour!=RED && ((current->left==NULL || current->left->colour!=RED) && (current->right==NULL || current->right->colour!=RED)))
				{
					//não há problemas com vermelhos. se estivermos num nó vermelho, é isolado
					return;
				}
				else if(current->colour == RED && current->parent->colour==RED)
				{
					//se tanto o nó atual como o pai são vermelhos, passa o problema para o nó de cima
					current = current->parent;
				}
				else if(this->getSibling(current)!=NULL && this->getSibling(current)->colour==RED)
				{
					//Current's sibling is red
					//o irmão deste nó é vermelho. muda a cor de ambos para preto, muda a cor do pai para vermelho, e continua a partir do pai
					current->colour = BLACK;
					this->getSibling(current)->colour = BLACK;
					if(current->parent!=this->root)
					{
						current->parent->colour = RED;
					}
					current = current->parent;
				}
				else if(current->left!=NULL && current->left->colour == RED)
				{
					//o filho esquerdo é vermelho
					if(current->parent!=NULL && current->parent->right==current)
					{
						//Current's sibling is black, current's red child is on same side as parent (left)
						//irmão do nó atual é preto ou inexistente, filho vermelho e pai estão do mesmo lado do nó atual -> dupla rotação com mudança de cor
						this->rotateLeft(current);
						this->rotateRight(current->parent->parent);
						current->parent->colour = BLACK;
						this->getSibling(current)->colour = RED;
						return;
					}
					else if(current->parent!=NULL && current->parent->left==current)
					{
						//Current's sibling is black, current's red child is on opposite side as parent (left child)
						//irmão do nó atual é preto ou inexistente, filho vermelho e pai estão em lados opostos -> rotação simples com mudança de cor
						this->rotateLeft(current->parent);
						current->colour = BLACK;
						current->right->colour = RED;
						return;
					}
				}
				else if(current->right!=NULL && current->right->colour == RED)
				{
					//o filho direito é vermelho
					if(current->parent!=NULL && current->parent->left==current)
					{
						//Current's sibling is black, current's red child is on same side as parent (right)
						//irmão do nó atual é preto ou inexistente, filho vermelho e pai estão do mesmo lado do nó atual -> dupla rotação com mudança de cor
						this->rotateRight(current);
						this->rotateLeft(current->parent->parent);
						current->parent->colour = BLACK;
						this->getSibling(current)->colour = RED;
						return;
					}
					else if(current->parent!=NULL && current->parent->right==current)
					{
						//Current's sibling is black, current's red child is on opposite side as parent (right child)
						//irmão do nó atual é preto ou inexistente, filho vermelho e pai estão em lados opostos -> rotação simples com mudança de cor
						this->rotateRight(current->parent);
						current->colour = BLACK;
						current->left->colour = RED;
						return;
					}
				}
				else
				{
					return;
				}
			}
		}
		void checkBalanceDeletion(node* leaf)			//função que verifica o equilíbrio da árvore após eliminação de um nó
		{
			if(leaf==NULL || leaf==this->root || leaf->colour==RED)			//verificação de erro + raiz + se o nó for vermelho
			{
				return;
			}
			else if(leaf->colour==BLACK)
			{
				//se o nó a eliminar for preto
				if(leaf->left==NULL && leaf->right==NULL)
				{
					//se for folha preta
					node* sibling = this->getSibling(leaf);
					if(sibling!=NULL && sibling->colour==RED)
					{
						//se o irmão for vermelho, alterar cores e fazer rotação do pai com o irmão. continuar verificação
						if(leaf->parent!=NULL)
						{
							leaf->parent->colour=RED;
							sibling->colour=BLACK;
							if(leaf->parent->right==leaf)
							{
								this->rotateLeft(leaf->parent);
							}
							else if(leaf->parent->left==leaf)
							{
								this->rotateRight(leaf->parent);
							}
							this->checkBalanceDeletion(leaf);
							return;
						}
					}
					else if(sibling!=NULL && sibling->colour==BLACK)
					{
						//se o irmão for preto
						if((sibling->left!=NULL && sibling->left->colour==RED) || (sibling->right!=NULL && sibling->right->colour==RED))
						{
							//se irmão tem pelo menos um sobrinho vermelho
							//se nao houver sobrinho "afastado", alterar cor do sobrinho mais próximo para preto, e fazer rotação dupla com esse lado
							//se sobrinho afastado for preto, fazer uma rotação simples com o outro sobrinho
							//por último, mudar cor do sobrinho afastado para preto, cor do irmão para a cor do pai, e cor do pai para preto.
							//proceder à "rotação final" do irmão, com o sobrinho "afastado" 
							if(sibling->parent->left==sibling)
							{
								node* farNephew = sibling->left;
								if(farNephew==NULL)
								{
									sibling->right->colour = BLACK;
									this->rotateRight(sibling);
									this->rotateLeft(this->getSibling(leaf));
									return;
								}
								else if(farNephew->colour==BLACK)
								{
									this->rotateRight(sibling);
								}
								sibling=this->getSibling(leaf);
								farNephew=sibling->left;
								farNephew->colour=BLACK;
								sibling->colour=sibling->parent->colour;
								leaf->parent->colour=BLACK;
								this->rotateLeft(leaf->parent);
								return;
							}
							else if(sibling->parent->right==sibling)
							{
								node* farNephew = sibling->right;
								if(farNephew==NULL)
								{
									sibling->left->colour = BLACK;
									this->rotateLeft(sibling);
									this->rotateRight(this->getSibling(leaf));
									return;
								}
								else if(farNephew->colour==BLACK)
								{
									this->rotateLeft(sibling);
								}
								sibling=this->getSibling(leaf);
								farNephew=sibling->right;
								farNephew->colour=BLACK;
								sibling->colour=sibling->parent->colour;
								leaf->parent->colour=BLACK;
								this->rotateRight(leaf->parent);
								return;
							}
						}
						else
						{
							//se ambos os sobrinhos forem pretos, ou não os houver, alterar a cor do irmão para vermelho
							//se o pai for vermelho, mudar para preto e terminar. senão verificar equilíbrio a partir daí
							sibling->colour=RED;
							if(leaf->parent->colour==RED)
							{
								leaf->parent->colour=BLACK;
								return;
							}
							else
							{
								this->checkBalanceDeletion(leaf->parent);
								return;
							}
						}
					}
				}
				else if(leaf->right==NULL)
				{
					//se só tiver um filho, e for vermelho, muda a sua cor para preto. se já for preto, terminar.
					if(leaf->left->colour==RED)
					{
						leaf->left->colour=BLACK;
						return;
					}
					else
					{
						return;
					}
				}
				else if(leaf->left==NULL)
				{
					//se só tiver um filho, e for vermelho, muda a sua cor para preto. se já for preto, terminar.
					if(leaf->right->colour==RED)
					{
						leaf->right->colour=BLACK;
						return;
					}
					else
					{
						return;
					}
				}
			}
		}
		node* root;
};

int main(int argc, char* argv[])
{
	Btree tree;												//cria árvore
	string input;											//string para ler cada linha de input
	char* word = (char*)calloc(WORDSIZE,sizeof(char));		//string para cada palavra
	getline(cin,input);										//lê a primeira linha de input
	do
	{
		while(input.size() != 0)							//cada palavra é removida da linha de input, logo no fim a linha vai ter tamanho 0
		{
			char* line = (char*)input.c_str();				//"transforma" a string de C++ numa string clássica
			int result = sscanf(line,"%s",word);			//lê cada palavra
			if(result < 1 || result ==EOF)					//se já não há palavras na linha, passa à linha seguinte
			{
				continue;
			}
			input.erase(0,strlen(word) + 1);				//apaga a palavra lida da linha
			if(strcmp(word,"REMOVE") == 0)					//se for REMOVE, remove a palavra seguinte da árvore
			{
				do
				{
					int toRemove = sscanf(line,"%s",word);	//lê a palavra a remover, se existir
					if(toRemove < 1 || toRemove == EOF)		//se não exitir, REMOVE estava no fim da linha, logo não faz nada
					{
						break;
					}
					input.erase(0,strlen(word) + 1);		//apaga a palavra a remover da linha
				}while(strcmp(word,"REMOVE")==0);			//pode ser "REMOVE REMOVE x"
				tree.deleteNode(word);						//elimina a palavra da árvore binária
				//tree.printTree();
			}
			else
			{
				tree.insert(word);							//se não for REMOVE, adiciona a palavra à árvore, ou aumenta a sua contagem
				//tree.printTree();
			}
		}
		getline(cin,input);									//lê linha seguinte
	}
	while(input.size()!=0);									//ciclo corre até ler uma linha em branco
	free(word);												//liberta a memória reservada para as palavras
	tree.printTree();										//apresenta os resultados
	return 0;
}
